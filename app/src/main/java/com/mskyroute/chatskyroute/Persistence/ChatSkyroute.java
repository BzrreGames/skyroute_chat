package com.mskyroute.chatskyroute.Persistence;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

public class ChatSkyroute extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //Se activa la presistencia para el funcionamiento Offline
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
