package com.mskyroute.chatskyroute.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.text.emoji.widget.EmojiTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.mskyroute.chatskyroute.Entidades.MensajeRecibir;
import com.mskyroute.chatskyroute.R;

import java.lang.annotation.Target;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterMensaje extends RecyclerView.Adapter<AdapterMensaje.HolderMensaje> {

    private  List<MensajeRecibir> listMensajes = new ArrayList<>();
    private Context c;
    private OnImageClickListener imgListener;

    public AdapterMensaje(Context c) {
        this.c = c;
    }

    public void addMensaje(MensajeRecibir m){
        listMensajes.add(m);
        notifyItemInserted(listMensajes.size());
    }

    @Override
    public HolderMensaje onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.mensaje_cardview,parent,false);
        return new HolderMensaje(v);
    }

    @Override
    public void onBindViewHolder(HolderMensaje holder, final int position) {
        /**
         * se obtienen los parametros de posicion de cada objeto
         */
        RelativeLayout.LayoutParams cvParams = (RelativeLayout.LayoutParams) holder.cvMensaje.getLayoutParams();
        LinearLayout.LayoutParams bgParams = (LinearLayout.LayoutParams) holder.imgBg.getLayoutParams() ;
        LinearLayout.LayoutParams hrParams = (LinearLayout.LayoutParams) holder.txtHoraMensaje.getLayoutParams();
        LinearLayout.LayoutParams msjParams = (LinearLayout.LayoutParams) holder.txtMensaje.getLayoutParams();

        //Variables finales
        final ProgressBar progressBar = holder.progressBar;
        final ImageView imgView = holder.imgAdjunto;


        holder.txtMensaje.setText(listMensajes.get(position).getsMensaje());

        String tipoMsj = listMensajes.get(position).getsTipoMensaje();

        if(tipoMsj.indexOf("1_") != -1){//Msj de salida
            /**
             * Si el mensaje lo enviamos nostros se alinea a la izquierda
             */
            cvParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT,0);
            cvParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            bgParams.gravity = Gravity.RIGHT;
            msjParams.gravity = Gravity.RIGHT;
            hrParams.gravity = Gravity.RIGHT;

            holder.imgBg.setBackgroundResource(R.drawable.msj_new_out_2);

            switch(tipoMsj){

                case "1_1"://Tipo texto
                    holder.imgAdjunto.setVisibility(View.GONE);
                    holder.txtMensaje.setVisibility(View.VISIBLE);
                    break;

                case "1_2"://Tipo img
                    /**
                     * Mostramos un progressbar mientras carga la imagen
                     */
                    progressBar.setVisibility(View.VISIBLE);
                    holder.txtMensaje.setVisibility(View.VISIBLE);
                    Glide.with(c)
                            .load(listMensajes.get(position).getsUrlFoto())
                            //.placeholder(R.drawable.placeholder)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<GlideDrawable> target, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, com.bumptech.glide.request.target.Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    imgView.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(holder.imgAdjunto);

                    break;
            }

        }else{//Msj recibido
            /**
             * Si el mensaje lo hemos recibido se alinea a la derecha
             */
            cvParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
            cvParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            bgParams.gravity = Gravity.LEFT;
            msjParams.gravity = Gravity.LEFT;
            hrParams.gravity = Gravity.LEFT;

            holder.imgBg.setBackgroundResource(R.drawable.msj_new_in_2);

            switch(tipoMsj){

                case "2_1"://Tipo texto
                    holder.imgAdjunto.setVisibility(View.GONE);
                    holder.txtMensaje.setVisibility(View.VISIBLE);

                    break;
                case "2_2"://Tipo img
                    /**
                     * Se muestra el progress bar miestra carga la imagen
                     */
                    progressBar.setVisibility(View.VISIBLE);
                    holder.txtMensaje.setVisibility(View.VISIBLE);

                    Glide.with(c)
                            .load(listMensajes.get(position).getsUrlFoto())
                            //.placeholder(R.drawable.placeholder)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<GlideDrawable> target, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, com.bumptech.glide.request.target.Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    imgView.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(holder.imgAdjunto);

                    break;
            }

        }

        holder.cvMensaje.setLayoutParams(cvParams);
        holder.imgBg.setLayoutParams(bgParams) ;
        holder.txtHoraMensaje.setLayoutParams(hrParams);
        holder.txtMensaje.setLayoutParams(msjParams);

        Long codHora = listMensajes.get(position).getHora();
        Date fecha = new Date(codHora);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        holder.txtHoraMensaje.setText(format.format(fecha));

    }

    @Override
    public int getItemCount() {
        return listMensajes.size();
    }


    public class HolderMensaje extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener{

        CardView cvMensaje;
        LinearLayout imgBg;
        EmojiTextView txtMensaje;
        TextView txtHoraMensaje;
        CircleImageView fotoMensaje;
        ImageView imgAdjunto;
        ProgressBar progressBar;

        public HolderMensaje(View itemView){
            super(itemView);
            cvMensaje = (CardView) itemView.findViewById(R.id.cvMensaje) ;
            imgBg = (LinearLayout) itemView.findViewById(R.id.imgBg);
            txtMensaje = (EmojiTextView) itemView.findViewById(R.id.txtMensaje);
            txtHoraMensaje = (TextView) itemView.findViewById(R.id.txtHoraMensaje);
            fotoMensaje = (CircleImageView) itemView.findViewById(R.id.fotoMensaje);
            imgAdjunto = (ImageView) itemView.findViewById(R.id.imgAdjunto);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress);

            imgAdjunto.setOnClickListener(this);
            imgAdjunto.setOnCreateContextMenuListener(this);
        }

        /**
         * Implementamos el onclik de nuestra imagen
         * @param v la vista
         */
        @Override
        public void onClick(View v) {
            if(imgListener != null){
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION){
                    imgListener.onClick(listMensajes.get(position).getsUrlFoto());
                }

            }
        }

        /**
         * Metodo que crea el menu al dejar selecionada una imagen
         * @param menu
         * @param v
         * @param menuInfo
         */
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Selecciona");
            MenuItem downloadImg = menu.add(menu.NONE,1,1,"Descargar");
            downloadImg.setOnMenuItemClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if(imgListener != null){
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION){
                    switch(item.getItemId()){
                        case 1://Donwload Image
                            imgListener.onDownloadClick(position, listMensajes.get(position).getsUrlFoto());
                            return true;
                    }
                }
            }
            return false;
        }
    }

    public interface OnImageClickListener{
        //Metodo para descargar la imagen enviada o recibida
        void onDownloadClick(int position,String uriAdjunto);
        //Meododo para mostrar la imagen a tamaño real
        void onClick(String uriAdjunto);
    }

    public void setOnImageClickListener(OnImageClickListener listener){
        imgListener = listener;
    }
}
