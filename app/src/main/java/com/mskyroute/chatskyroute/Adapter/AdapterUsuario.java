package com.mskyroute.chatskyroute.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mskyroute.chatskyroute.Activity.AmigosActivity;
import com.mskyroute.chatskyroute.Activity.ChatActivity;
import com.mskyroute.chatskyroute.Entidades.Usuario;
import com.mskyroute.chatskyroute.R;
import com.mskyroute.chatskyroute.Util.StaticConstans;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterUsuario extends RecyclerView.Adapter<AdapterUsuario.HolderUsuario> {

    private List<UsuarioDTO> listaAmigos = new ArrayList<>();
    private Context c;

    public AdapterUsuario(Context c) {
        this.c = c;
    }

    public void updateUsuario(UsuarioDTO usu){
        for(UsuarioDTO u : listaAmigos){
            if(u.getKey().equals(usu.key)){
                u.setUsuario(usu.getUsuario());
            }
        }
    }

    public void addUsuario(UsuarioDTO usu){
        listaAmigos.add(usu);
        notifyItemInserted(listaAmigos.size());
    }

    @Override
    public HolderUsuario onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.amigo_cardview,parent,false);
        return new HolderUsuario(v);
    }

    @Override
    public void onBindViewHolder(HolderUsuario holder, final int position) {
        holder.nombreUsuario.setText(listaAmigos.get(position).usuario.getUserName().toString());
        holder.cvUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, ChatActivity.class);
                i.putExtra("emisor", StaticConstans.getStringConstan(c,StaticConstans.sNomUsuario));
                i.putExtra("receptor", listaAmigos.get(position).getUsuario().getUserName());
                i.putExtra("uriPhoto",listaAmigos.get(position).getUsuario().getsUriFoto());
                c.startActivity(i);
            }
        });

        Glide.with(c)
                    .load(listaAmigos.get(position).getUsuario().getsUriFoto())
                    .error(R.mipmap.ic_launcher)
                    .into(holder.fotoUsuario);

    }

    @Override
    public int getItemCount() {
        return listaAmigos.size();
    }

    public class HolderUsuario extends ViewHolder{
        CardView cvUsuario;
        CircleImageView fotoUsuario;
        TextView nombreUsuario;

        public HolderUsuario(View itemView) {
            super(itemView);
            cvUsuario = (CardView) itemView.findViewById(R.id.cvAmigo);
            fotoUsuario = (CircleImageView) itemView.findViewById(R.id.imgFotoAmigoL);
            nombreUsuario = (TextView) itemView.findViewById(R.id.txtNombreAmigoL);
        }
    }

    /**
     * Data Tranfer Objeto para la vista de los amigos
     * Objeto que nos almacena a los amigos con su repectiva key
     */
    public static class UsuarioDTO {

        private Usuario usuario;
        private String key;

        public UsuarioDTO(){
            this.usuario = new Usuario();
            this.key = "";
        }

        public Usuario getUsuario() {
            return usuario;
        }

        public void setUsuario(Usuario usuario) {
            this.usuario = usuario;
        }

        public String getKey() {
            return this.key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }
}
