package com.mskyroute.chatskyroute.Entidades;

public class MensajeRecibir extends Mensaje {

    private Long hora;

    public MensajeRecibir() {
    }

    public MensajeRecibir(Long hora) {
        this.hora = hora;
    }

    public MensajeRecibir(String sMensaje, String sTipoMensaje, Long hora) {
        super(sMensaje, sTipoMensaje);
        this.hora = hora;
    }

    public MensajeRecibir(String sMensaje, String sTipoMensaje, String uriAdjunto, Long hora) {
        super(sMensaje, sTipoMensaje, uriAdjunto);
        this.hora = hora;
    }

    public Long getHora() {
        return hora;
    }

    public void setHora(Long hora) {
        this.hora = hora;
    }
}
