package com.mskyroute.chatskyroute.Entidades;

public class Mensaje {

    private String sMensaje;
    private String sTipoMensaje;
    private String sUrlFoto;

    public Mensaje( ) {
    }

    public Mensaje(String sMensaje, String sTipoMensaje) {
        this.sMensaje = sMensaje;
        this.sTipoMensaje = sTipoMensaje;
        this.sUrlFoto = "";
    }

    public Mensaje(String sMensaje, String sTipoMensaje, String sUrlFoto) {
        this.sMensaje = sMensaje;
        this.sTipoMensaje = sTipoMensaje;
        this.sUrlFoto = sUrlFoto;
    }

    public String getsUrlFoto() {
        return sUrlFoto;
    }

    public void setsUrlFoto(String sUrlFoto) {
        this.sUrlFoto = sUrlFoto;
    }

    public String getsMensaje() {
        return sMensaje;
    }

    public void setsMensaje(String sMensaje) {
        this.sMensaje = sMensaje;
    }

    public String getsTipoMensaje() {
        return sTipoMensaje;
    }

    public void setsTipoMensaje(String sTipoMensaje) {
        this.sTipoMensaje = sTipoMensaje;
    }
}
