package com.mskyroute.chatskyroute.Entidades;

import java.util.Map;

public class MensajeEnviar extends Mensaje {
    private Map hora;

    public MensajeEnviar(){}

    public MensajeEnviar(Map hora){
        this.hora = hora;
    }

    public MensajeEnviar(String sMensaje, String sTipoMensaje, Map hora) {
        super(sMensaje, sTipoMensaje);
        this.hora = hora;
    }

    public MensajeEnviar(String sMensaje, String sTipoMensaje, String uriAdjunto, Map hora) {
        super(sMensaje, sTipoMensaje, uriAdjunto);
        this.hora = hora;
    }

    public Map getHora() {
        return hora;
    }

    public void setHora(Map hora) {
        this.hora = hora;
    }
}
