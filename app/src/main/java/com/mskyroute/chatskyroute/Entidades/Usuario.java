package com.mskyroute.chatskyroute.Entidades;

public class Usuario {

    private String userName;
    private String email;
    private String sUriFoto;


    public Usuario(){
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getsUriFoto() {
        return sUriFoto;
    }

    public void setsUriFoto(String sUriFoto) {
        this.sUriFoto = sUriFoto;
    }
}
