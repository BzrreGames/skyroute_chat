package com.mskyroute.chatskyroute.Activity;

        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Color;
        import android.support.annotation.NonNull;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.text.InputType;
        import android.text.SpannableString;
        import android.text.Spanned;
        import android.text.style.RelativeSizeSpan;
        import android.text.style.TypefaceSpan;
        import android.util.TypedValue;
        import android.view.Gravity;
        import android.view.View;
        import android.view.inputmethod.EditorInfo;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.TableLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.google.android.gms.tasks.OnCompleteListener;
        import com.google.android.gms.tasks.Task;
        import com.google.firebase.auth.AuthResult;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.auth.FirebaseUser;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.ValueEventListener;
        import com.mskyroute.chatskyroute.Entidades.Usuario;
        import com.mskyroute.chatskyroute.R;
        import com.mskyroute.chatskyroute.Util.StaticConstans;

        import java.util.regex.Matcher;
        import java.util.regex.Pattern;

        import static com.mskyroute.chatskyroute.R.color.colorButton;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    //* Objetos de la vista
    ImageView imgLogo;
    EditText txtEmail;
    EditText txtPass;
    Button btnLgn;
    TextView opRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        RelativeLayout rlLayout = new RelativeLayout(this);
        LinearLayout lnLayout = new LinearLayout(this);
        imgLogo = new ImageView(this);
        txtEmail = new EditText(this);
        txtPass = new EditText(this);
        btnLgn = new Button(this);
        opRegistrar= new TextView(this);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        LinearLayout.LayoutParams layoutParams;

        //add Properties relative layout
        layoutParams = new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.MATCH_PARENT);
        rlLayout.setLayoutParams(layoutParams);
        rlLayout.setGravity(Gravity.CENTER);

        //LinearLayout, orientacion vertical, gravity center
        layoutParams = new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        lnLayout.setLayoutParams(layoutParams);
        lnLayout.setOrientation(LinearLayout.VERTICAL);
        lnLayout.setGravity(Gravity.CENTER);

        //Padding
        int horizontalPadding = (int) getTypedValueInDP(this, 10);
        int verticalPadding = (int) getTypedValueInDP(this, 10);
        rlLayout.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);


        layoutParams = new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        imgLogo.setLayoutParams(layoutParams);
        imgLogo.setImageResource(R.drawable.logo_chat);

        SpannableString hintUsr = new SpannableString("Email" );
        hintUsr.setSpan(new RelativeSizeSpan(0.9f), 0, hintUsr.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        hintUsr.setSpan(new TypefaceSpan("sans-serif"), 0, hintUsr.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtEmail.setHint(hintUsr);
        txtEmail.setText("@gmail.com");

        SpannableString hintPwd = new SpannableString("Contraseña" );
        hintPwd.setSpan(new RelativeSizeSpan(0.9f), 0, hintPwd.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        hintPwd.setSpan(new TypefaceSpan("sans-serif"), 0, hintPwd.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtPass.setHint(hintPwd);
        txtPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        txtPass.setInputType(InputType.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);

        opRegistrar.setText("Registrarse");
        opRegistrar.setGravity(Gravity.CENTER);
        opRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarUsuario();
            }
        });

        btnLgn.setText("Ingresar");
        btnLgn.setBackgroundColor(getResources().getColor(R.color.colorButton));
        btnLgn.setTextColor(Color.WHITE);
        btnLgn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Login();
            }
        });

        //add elements
        lnLayout.addView(imgLogo);
        lnLayout.addView(txtEmail);
        lnLayout.addView(txtPass);
        lnLayout.addView(btnLgn);
        lnLayout.addView(opRegistrar);
        rlLayout.addView(lnLayout);

        setContentView(rlLayout);

    }


    private void Login(){
        String userMail = txtEmail.getText().toString();
        String userPass = txtPass.getText().toString();
        if(validarEmail(userMail) && validarPassword(userPass)){
            mAuth.signInWithEmailAndPassword(userMail, userPass)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Toast.makeText(LoginActivity.this, "Bienvenido",Toast.LENGTH_SHORT).show();
                                isLoggin();
                            } else {
                                // If sign in fails, display a message to the user.
                                Toast.makeText(LoginActivity.this, "Datos incorrectos...",Toast.LENGTH_SHORT).show();
                            }

                            // ...
                        }
                    });
        }
    }

    private void registrarUsuario(){
        startActivity(new Intent(LoginActivity.this,RegistroActivity.class));
    }

    private boolean validarEmail(String email){
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
        if (mather.find() == true) {
            return true;
        }else{
            Toast.makeText(this,"Email no valido", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void nextActivity(){
        startActivity(new Intent(LoginActivity.this,AmigosActivity.class));
        finish();
    }

    private boolean validarPassword(String pass){
        if(!pass.isEmpty() && pass.length()>=6){
            return true;
        }
        Toast.makeText(this,"Contraseña no valida", Toast.LENGTH_LONG).show();
        return false;
    }

    private float getTypedValueInDP(Context context, float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.getResources().getDisplayMetrics());
    }

    private void isLoggin(){
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            DatabaseReference reference = database.getReference("Usuarios/"+currentUser.getUid());
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Usuario usr = dataSnapshot.getValue(Usuario.class);
                    StaticConstans.setStringConstan(LoginActivity.this,StaticConstans.sNomUsuario,usr.getUserName());
                    StaticConstans.setStringConstan(LoginActivity.this,StaticConstans.sUriFoto,usr.getsUriFoto());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            nextActivity();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isLoggin();
    }
}
