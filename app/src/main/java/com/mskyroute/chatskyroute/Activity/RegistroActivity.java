package com.mskyroute.chatskyroute.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mskyroute.chatskyroute.Entidades.Usuario;
import com.mskyroute.chatskyroute.Util.StaticConstans;


public class RegistroActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference usuariosDB;

    //View objects
    Toolbar toolBar;
    EditText txtUsername;
    EditText txtMail;
    EditText txtPass;
    EditText txtRepass;
    Button btnRegistrar;

    @Override
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        RelativeLayout rlLayout = new RelativeLayout(this);
        LinearLayout lnLayout = new LinearLayout(this);
        AppBarLayout barLayout = new AppBarLayout(this);
        toolBar = new Toolbar(this);
        txtUsername = new EditText(this);
        txtMail = new EditText(this);
        txtPass = new EditText(this);
        txtRepass = new EditText(this);
        btnRegistrar = new Button(this);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        LinearLayout.LayoutParams layoutParams;


        //add Properties relative layout
        layoutParams = new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.MATCH_PARENT);
        rlLayout.setLayoutParams(layoutParams);
        rlLayout.setGravity(Gravity.CENTER);

        //add Properties Linear layout
        layoutParams = new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        lnLayout.setLayoutParams(layoutParams);
        lnLayout.setOrientation(LinearLayout.VERTICAL);
        lnLayout.setGravity(Gravity.CENTER);

        //Padding relative layout
        int horizontalPadding = (int) getTypedValueInDP(this, 10);
        int verticalPadding = (int) getTypedValueInDP(this, 10);
        rlLayout.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);

        //AppBarLayout
        layoutParams = new LinearLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
        barLayout.setLayoutParams(layoutParams);

        //ToolBar



        SpannableString hintUsr = new SpannableString("Usuario" );
        hintUsr.setSpan(new RelativeSizeSpan(0.9f), 0, hintUsr.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        hintUsr.setSpan(new TypefaceSpan("sans-serif"), 0, hintUsr.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtUsername.setHint(hintUsr);

        SpannableString hintMail = new SpannableString("Email" );
        hintMail.setSpan(new RelativeSizeSpan(0.9f), 0, hintMail.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        hintMail.setSpan(new TypefaceSpan("sans-serif"), 0, hintMail.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtMail.setHint(hintMail);

        SpannableString hintPass = new SpannableString("Contraseña" );
        hintPass.setSpan(new RelativeSizeSpan(0.9f), 0, hintPass.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        hintPass.setSpan(new TypefaceSpan("sans-serif"), 0, hintPass.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtPass.setHint(hintPass);
        txtPass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        txtPass.setInputType(InputType.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);

        SpannableString hintRepass = new SpannableString("Repetir contraseña" );
        hintRepass.setSpan(new RelativeSizeSpan(0.9f), 0, hintRepass.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        hintRepass.setSpan(new TypefaceSpan("sans-serif"), 0, hintRepass.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        txtRepass.setHint(hintRepass);
        txtRepass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        txtRepass.setInputType(InputType.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);

        btnRegistrar.setText("Registrar");
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                registrar();
            }
        });

        lnLayout.addView(txtUsername);
        lnLayout.addView(txtMail);
        lnLayout.addView(txtPass);
        lnLayout.addView(txtRepass);
        lnLayout.addView(btnRegistrar);

        rlLayout.addView(lnLayout);
        setContentView(rlLayout);
    }

    private void registrar(){
        final String userMail = txtMail.getText().toString();
        if(validarCampos() && validarEmail(userMail)){
            final String userName = txtUsername.getText().toString();
            String pass = txtPass.getText().toString();

            mAuth.createUserWithEmailAndPassword(userMail, pass)
                    .addOnCompleteListener(RegistroActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Usuario usu = new Usuario();
                                usu.setUserName(userName);
                                usu.setEmail(userMail);
                                usu.setsUriFoto("");
                                FirebaseUser currentUser = mAuth.getCurrentUser();
                                DatabaseReference reference = database.getReference("Usuarios/"+currentUser.getUid());
                                reference.setValue(usu);
                                StaticConstans.setStringConstan(RegistroActivity.this,StaticConstans.sNomUsuario,usu.getUserName());
                                StaticConstans.setStringConstan(RegistroActivity.this,StaticConstans.sUriFoto,usu.getsUriFoto());
                                Toast.makeText(RegistroActivity.this,"Se registro correctamente", Toast.LENGTH_LONG).show();
                                nextActivity();
                            } else {
                                // If sign in fails, display a message to the user.
                                Toast.makeText(RegistroActivity.this,"Error al registrarse", Toast.LENGTH_LONG).show();
                            }

                            // ...
                        }
                    });


        }else{
            Toast.makeText(RegistroActivity.this,"Datos no validos", Toast.LENGTH_LONG).show();
        }

    }

    private boolean validarEmail(String email){
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
        if (mather.find() == true) {
            return true;
        }else{
            Toast.makeText(RegistroActivity.this,"Email no valido", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void nextActivity(){
        startActivity(new Intent(RegistroActivity.this,LoginActivity.class));
        finish();
    }

    private boolean validarCampos(){
        String user = txtUsername.getText().toString();
        user.trim();
        if(!user.equals("") && !user.isEmpty()){
            String pass = txtPass.getText().toString();
            String rePass = txtRepass.getText().toString();
            if(pass.equals(rePass) && !pass.isEmpty()){
                return true;
            }
            return false;
        }
        return false;
    }

    private float getTypedValueInDP(Context context, float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.getResources().getDisplayMetrics());
    }
}
