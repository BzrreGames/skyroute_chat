package com.mskyroute.chatskyroute.Activity;



import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.text.emoji.EmojiCompat;
import android.support.text.emoji.bundled.BundledEmojiCompatConfig;
import android.support.text.emoji.widget.EmojiEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mskyroute.chatskyroute.R;

import com.mskyroute.chatskyroute.Adapter.AdapterMensaje;
import com.mskyroute.chatskyroute.Entidades.MensajeEnviar;
import com.mskyroute.chatskyroute.Entidades.MensajeRecibir;
import com.mskyroute.chatskyroute.Util.StaticConstans;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity implements AdapterMensaje.OnImageClickListener{

    private static final int CODE_ADJUNTO = 505;

    private AdapterMensaje adapter;
    private FirebaseDatabase database;
    private DatabaseReference chatDB;
    private DatabaseReference chatReceptorDB;
    private FirebaseStorage storage;
    private StorageReference carpetaAdjuntos;

    private String sEmisor;
    private String sReceptor;
    private String sUriFoto;

    private String TAG ="[Chat Activity]";

    CircleImageView fotoAmigo;
    TextView txtNombreAmigo;
    RecyclerView rvMensajes;
    EditText txtMensajeEnviar;
    ImageButton btnAdjunto;
    Button btnEnviar;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //INIT de EMOJIS
        EmojiCompat.Config config = new BundledEmojiCompatConfig(this);
        EmojiCompat.init(config);

        setContentView(R.layout.activity_chat);
        init();
    }

    private void init(){
        //Refenciamos los objetos de la vista
        fotoAmigo = (CircleImageView) findViewById(R.id.fotoAmigo);
        txtNombreAmigo = (TextView) findViewById(R.id.txtNombreAmigo);
        rvMensajes = (RecyclerView) findViewById(R.id.rvMensajes);
        txtMensajeEnviar = (EmojiEditText) findViewById(R.id.txtMensajeEnviar);
        btnAdjunto = (ImageButton) findViewById(R.id.btnAdjunto);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        Toolbar toolAtras = (Toolbar) findViewById(R.id.toolAtras);
        progressDialog = new ProgressDialog(this);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if(bundle!= null){
            sEmisor = bundle.getString("emisor");
            sReceptor = bundle.getString("receptor");
            sUriFoto = bundle.getString("uriPhoto");
        }else{
            sEmisor = "yo";
            sReceptor = "destino";
        }

        if(!sUriFoto.equalsIgnoreCase("")){
            Glide.with(this).load(sUriFoto).into(fotoAmigo);
        }

        txtNombreAmigo.setText(sReceptor);//Set el nombre del receptor

        /**
         * Cargamos las referencias para las dos salas de chat
         *  Emisor_Receptor
         *  Receptor_Emisor
         */
        database = FirebaseDatabase.getInstance();
        chatDB = database.getReference(sEmisor+"_"+sReceptor);
        chatReceptorDB = database.getReference(sReceptor+"_"+sEmisor);
        chatDB.keepSynced(true);//Se activa la funcionalida offline
        chatReceptorDB.keepSynced(true);//Se activa la funcionalidad offline

        //Se Crea el objeto para almacenare archivos
        storage = FirebaseStorage.getInstance();

        adapter = new AdapterMensaje(this);
        LinearLayoutManager l =  new LinearLayoutManager(this);
        rvMensajes.setLayoutManager(l);
        rvMensajes.setAdapter(adapter);
        adapter.setOnImageClickListener(this);

        toolAtras.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarMensaje();
            }
        });

        btnAdjunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAjunto();
            }
        });


        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScrollbar();
            }
        });

        chatDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MensajeRecibir msj = dataSnapshot.getValue(MensajeRecibir.class);
                adapter.addMensaje(msj);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void enviarMensaje(){
        String sMensaje = txtMensajeEnviar.getText().toString();
        sMensaje = sMensaje.trim();
        if(!sMensaje.isEmpty()){
            MensajeEnviar msj = new MensajeEnviar(sMensaje, "1_1", ServerValue.TIMESTAMP);
            chatDB.push().setValue(msj);
            msj.setsTipoMensaje("2_1");
            chatReceptorDB.push().setValue(msj);
            txtMensajeEnviar.setText("");

            InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(txtMensajeEnviar.getWindowToken(), 0);
        }
    }

    private void addAjunto(){
        if(StaticConstans.verifyStoragePermissions(this)){
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(i, "Seleciona una foto"), CODE_ADJUNTO);
        }
    }

    private void setScrollbar(){
        rvMensajes.scrollToPosition(adapter.getItemCount()-1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CODE_ADJUNTO && resultCode == RESULT_OK){
            /**
             * Se inicia el dialogo de se esta subiendo una imagen
             */
            progressDialog.setTitle("Subiendo...");
            progressDialog.setMessage(null);
            progressDialog.show();

            Uri u = data.getData();
            carpetaAdjuntos = storage.getReference("files_");
            StorageReference fotoRef = carpetaAdjuntos.child(u.getLastPathSegment());
            fotoRef.putFile(u).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());
                    Uri u = urlTask.getResult();
                    MensajeEnviar msj = new MensajeEnviar(" Has enviado una imagen","1_2", u.toString(),ServerValue.TIMESTAMP);

                    /**
                     * Cierra cuadro de dialogo
                     */
                    progressDialog.dismiss();

                    chatDB.push().setValue(msj);
                    msj.setsTipoMensaje("2_2");
                    chatReceptorDB.push().setValue(msj);
                    Toast.makeText(ChatActivity.this, "Imagen enviada", Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    progressDialog.dismiss();
                    Toast.makeText(ChatActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                }
                /**
                 * Durante el progrso se va mostrando el proggreso de la subida de archivos
                 */
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    // percentage in progress dialog
                    progressDialog.setMessage("Cargando " + ((int) progress) + "%...");
                }
            });
        }
    }

    @Override
    public void onDownloadClick(int position, String uriAdjunto) {
        if(StaticConstans.verifyStoragePermissions(this)){
            StorageReference fileRef = storage.getReferenceFromUrl(uriAdjunto);
            if (fileRef != null) {
                /**
                 * Se carga el cuadro de dialogo
                 */
                progressDialog.setTitle("Descargando...");
                progressDialog.setMessage(null);
                progressDialog.show();

                try {
                    String nameFile = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(new Date());//Timestamp
                    File pathDir = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), "/Skyroutechat");
                    if (!pathDir.exists()) {
                        pathDir.mkdir();
                    }
                    nameFile = Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES) + "/Skyroutechat/Sky_" + nameFile + ".jpg";
                    final File localFile = new File(nameFile);

                    fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(ChatActivity.this, "Descarga completa", Toast.LENGTH_LONG).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(ChatActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            // progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            // percentage in progress dialog
                            progressDialog.setMessage("Descargando " + ((int) progress) + "%...");
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(ChatActivity.this, "No hay conexion", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(String uriAdjunto) {
        Dialog popImage = new Dialog(this);
        popImage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popImage.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK)
        );
        popImage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //nothing
            }
        });



        ImageView imgView = new ImageView(this);
        imgView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        Glide.with(this)
                .load(uriAdjunto)
                .placeholder(R.drawable.placeholder)
                .into(imgView);

        popImage.addContentView(imgView, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
        popImage.show();;
    }
}
