package com.mskyroute.chatskyroute.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mskyroute.chatskyroute.Adapter.AdapterUsuario;
import com.mskyroute.chatskyroute.Entidades.Usuario;
import com.mskyroute.chatskyroute.R;
import com.mskyroute.chatskyroute.Util.StaticConstans;

import de.hdodenhof.circleimageview.CircleImageView;

public class AmigosActivity extends AppCompatActivity {

    private final int CODE_FOTO = 8002;

    private Usuario USER;

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference usuariosDB;
    private FirebaseStorage storage;
    private StorageReference carpetaFotos;
    private DatabaseReference userBD;
    private FirebaseUser userLogin;

    private AdapterUsuario adapter;

    RecyclerView rvAmigos;
    CircleImageView fotoUsuario;
    TextView nombreUsuario;

    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        fireBaseInit();
        setContentView(R.layout.activity_amigos);
        init();
    }

    private void fireBaseInit(){
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        usuariosDB = database.getReference("Usuarios");
        storage = FirebaseStorage.getInstance();
        userLogin = mAuth.getCurrentUser();
        userBD = database.getReference("Usuarios/"+userLogin.getUid());
    }

    private void init(){
        fotoUsuario = (CircleImageView) findViewById(R.id.fotoUsuario) ;
        nombreUsuario = (TextView) findViewById(R.id.txtNombreUsuario);
        rvAmigos = (RecyclerView) findViewById(R.id.rvAmigos);
        progressDialog = new ProgressDialog(this);

        adapter = new AdapterUsuario(this);
        LinearLayoutManager l =  new LinearLayoutManager(this);
        rvAmigos.setLayoutManager(l);
        rvAmigos.setAdapter(adapter);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolUser);
        setSupportActionBar(myToolbar);

        userBD.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String newUriFoto = (String)dataSnapshot.getValue();
                Glide.with(AmigosActivity.this)
                        .load(newUriFoto)
                        .error(R.mipmap.ic_launcher)
                        .into(fotoUsuario);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        usuariosDB.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Usuario usu = dataSnapshot.getValue(Usuario.class);
                if(!usu.getUserName().equalsIgnoreCase(StaticConstans.getStringConstan(AmigosActivity.this,StaticConstans.sNomUsuario))){
                    AdapterUsuario.UsuarioDTO amigo = new AdapterUsuario.UsuarioDTO();
                    amigo.setUsuario(usu);
                    amigo.setKey(dataSnapshot.getKey());
                    adapter.addUsuario(amigo);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Usuario usu = dataSnapshot.getValue(Usuario.class);
                if(!usu.getUserName().equalsIgnoreCase(StaticConstans.getStringConstan(AmigosActivity.this,StaticConstans.sNomUsuario))){
                    AdapterUsuario.UsuarioDTO amigo = new AdapterUsuario.UsuarioDTO();
                    amigo.setUsuario(usu);
                    amigo.setKey(dataSnapshot.getKey());
                    adapter.updateUsuario(amigo);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void regresarActivity(){
        startActivity(new Intent(AmigosActivity.this, LoginActivity.class));
        finish();
    }

    private void isLoggin(){
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            regresarActivity();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.optCambiarFoto:
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(Intent.createChooser(i, "Seleciona una foto"), CODE_FOTO);
                break;
            case R.id.optCerrarSesion:
                FirebaseAuth.getInstance().signOut();
                StaticConstans.setStringConstan(this, StaticConstans.sNomUsuario,"");
                StaticConstans.setStringConstan(this, StaticConstans.sUriFoto," ");
                regresarActivity();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String usr = StaticConstans.getStringConstan(AmigosActivity.this,StaticConstans.sNomUsuario);
        String uriFoto = StaticConstans.getStringConstan(AmigosActivity.this,StaticConstans.sUriFoto);
        nombreUsuario.setText(usr);
        Glide.with(AmigosActivity.this)
                .load(uriFoto)
                .error(R.mipmap.ic_launcher)
                .into(fotoUsuario);
    }
    @Override
    protected void onResume() {
        super.onResume();
        isLoggin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CODE_FOTO && resultCode == RESULT_OK){
            /**
             * Se inicia el dialogo de se esta subiendo una imagen
             */
            progressDialog.setTitle("Subiendo...");
            progressDialog.setMessage(null);
            progressDialog.show();

            Uri u = data.getData();
            carpetaFotos = storage.getReference("photos");
            final StorageReference fotoRef = carpetaFotos.child(u.getLastPathSegment());
            fotoRef.putFile(u).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());
                    final Uri u = urlTask.getResult();
                    progressDialog.dismiss();

                    userBD.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Usuario usr = dataSnapshot.getValue(Usuario.class);
                            usr.setsUriFoto(u.toString());
                            userBD.setValue(usr);
                            StaticConstans.setStringConstan(AmigosActivity.this,StaticConstans.sUriFoto,usr.getsUriFoto());
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    // progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    // percentage in progress dialog
                    progressDialog.setMessage("Cargando " + ((int) progress) + "%...");
                }
            });
        }
    }

}
