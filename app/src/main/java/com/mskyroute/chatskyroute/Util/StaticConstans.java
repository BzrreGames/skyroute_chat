package com.mskyroute.chatskyroute.Util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class StaticConstans {

    public static  final String sConfig = "com.voices.chatsyroute";
    public static final String sNomUsuario = "";
    public static final String sUriFoto = " ";

    public static String getStringConstan(Context c, String key){
        SharedPreferences preferences = c.getSharedPreferences(sConfig, MODE_PRIVATE);
        return preferences.getString(key,"");
    }

    public static void setStringConstan(Context c, String key, String cad){
        SharedPreferences preferences = c.getSharedPreferences(sConfig, MODE_PRIVATE);
        preferences.edit().putString(key, cad).apply();
    }

    public static boolean verifyStoragePermissions(Activity activity) {
        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        int REQUEST_EXTERNAL_STORAGE = 1;
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            return false;
        }else{
            return true;
        }
    }


}
